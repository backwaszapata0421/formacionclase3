package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Saludocontroller {

    @RequestMapping("/saludo")
    public String index() {
        return "Hola, soy tu API";
    }
}
