package com.example.demo;

//import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@SpringBootTest
//@AutoConfigureMockMvc
public class SaludoControllerTest {

//    @Test
    public void firstTest() {
        System.out.println("Primer test");
    }

//    @Autowired
    private MockMvc mockMvc;

//    @Test
    public void getSaludo() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/saludo")).andExpect(status().isOk());
    }
}
