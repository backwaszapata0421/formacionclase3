package net.techu.data;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductoRepository extends MongoRepository<ProductoMongo, String> {

    /* solo si se garantiza que retorna un Unico elemento, si no, toca por lista
    @Query("{'nombre':?0}")
    public ProductoMongo findByNombre(String nombre); */
    @Query("{'nombre':?0}")
    public List<ProductoMongo> findByNombre(String nombre);

    @Query("{'nombre': {$gt: ?0, $lt: ?1}}")
    public List<ProductoMongo> findByPrecio(double minimo, double maximo);
}
