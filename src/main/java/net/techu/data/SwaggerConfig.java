package net.techu.data;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket apiDocket(){
        //Detalle de los metodos de la api
        // .apis(RequestHandlerSelectors.basePackage("net.techu.apicurso"))
        // .apis(RequestHandlerSelectors.basePackage("net.techu.data"))
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("net.techu.data"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(getApiInfo());
    }

    private ApiInfo getApiInfo () {
        return new ApiInfo("API de productos Controller",
                "API que gestiona productos V4 e integra Swagger",
                "0.0.2",
                "http://www.techu.net",
                new Contact("Walther", "www.walther.com", "walther@correo.es"),
                "LICENCIA",
                "URL LICENCIA",
                Collections.emptyList());
    }

}

