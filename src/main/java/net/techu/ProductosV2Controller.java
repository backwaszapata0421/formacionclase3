package net.techu;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductosV2Controller {
    private ArrayList<Producto> listaProductos = null;
    public ProductosV2Controller() {
        listaProductos = new ArrayList<>();
        listaProductos.add(new Producto(1, "PR1",27.35));
        listaProductos.add(new Producto(2, "PR2", 18.33));
    }
    /* Get lista de productos */
    @GetMapping(value = "/V2/productos", produces = "application/json")
    public ResponseEntity<List<Producto>> obtenerListado()
    {
        System.out.println("Estoy en obtener");
        return new ResponseEntity<>(listaProductos, HttpStatus.OK);
    }
    @GetMapping("/V2/productos/{id}")
    public ResponseEntity<Producto> obtenerProductoPorId(@PathVariable int id)
    {
        Producto resultado = null;
        ResponseEntity<Producto> respuesta = null;
        try
        {
            resultado = listaProductos.get(id);
            respuesta = new ResponseEntity<>(resultado, HttpStatus.OK);
        }
        catch (Exception ex)
        {
            //String mensaje = "No se ha encontrado el producto";
            //respuesta = new ResponseEntity(mensaje, HttpStatus.NOT_FOUND);
            respuesta = new ResponseEntity(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }
    /* Add nuevo producto */
    @PostMapping(value = "/V2/productos", produces="application/json")
    public ResponseEntity<String> addProducto(@RequestBody Producto productoNuevo) {
        System.out.println("Estoy en añadir");
        System.out.println(productoNuevo.getId());
        System.out.println(productoNuevo.getNombre());
        System.out.println(productoNuevo.getPrecio());
        //listaProductos.add(new Producto(99, nombre, 100.5));
        listaProductos.add(productoNuevo);
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }
    /* Add nuevo producto con nombre */
    @PostMapping(value = "/V2/productos/{nom}/{cat}", produces="application/json")
    public ResponseEntity<String> addProductoConNombre(@PathVariable("nom") String nombre, @PathVariable("cat") String categoria) {
        System.out.println("Voy a añadir el producto con nombre " + nombre + " y categoría " + categoria);
        listaProductos.add(new Producto(99, "NUEVO", 100.5));
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }
    /* con put se envia todos los campos */
    /* con Patch se modifica un solo campo */
    @PutMapping("/V2/productos/{id}")
    public ResponseEntity<String> updateProducto(@PathVariable int id, @RequestBody Producto productoModificado)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            Producto productoAModificar = listaProductos.get(id);
            System.out.println("voy a modificar el producto");
            System.out.println("Precio Actual: " + String.valueOf(productoAModificar.getPrecio()));
            System.out.println("nombre Actual: " + String.valueOf(productoAModificar.getNombre()));
            System.out.println("Precio Nuevo:  " + String.valueOf(productoModificado.getPrecio()));
            System.out.println("nombre Nuevo:  " + String.valueOf(productoModificado.getNombre()));
            productoAModificar.setNombre(productoModificado.getNombre());
            productoAModificar.setPrecio(productoModificado.getPrecio());
            listaProductos.set(id, productoAModificar);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }
    @PutMapping("/v2/subirprecio")
    public ResponseEntity<String> subirPrecio()
    {
        ResponseEntity<String> resultado = null;
        for (Producto p:listaProductos) {
            p.setPrecio(p.getPrecio()*1.25);
        }
        resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return resultado;
    }


    @DeleteMapping("/V2/productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable int id, @RequestBody Producto productoBorrado)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            Producto productoAEliminar = listaProductos.get(id);
            System.out.println("voy a Borrar el producto");
            System.out.println("Precio a Borrar: " + String.valueOf(productoAEliminar.getPrecio()));
            System.out.println("Nombre a Borrar: " + String.valueOf(productoAEliminar.getNombre()));
            listaProductos.remove(id);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }
    //BorraJson
    @DeleteMapping("/V2/productosjson")
    public ResponseEntity<String> deleteProductoJ(@RequestBody Producto productoBorrado)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            Producto productoAEliminar = listaProductos.get(productoBorrado.getId());
            System.out.println("voy a Borrar el producto");
            System.out.println("Precio a Borrar: " + String.valueOf(productoAEliminar.getPrecio()));
            System.out.println("Nombre a Borrar: " + String.valueOf(productoAEliminar.getNombre()));
            listaProductos.remove(productoAEliminar.getId()-1);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }
    // Borro todos
    @DeleteMapping("/v2/productos/d")
    public ResponseEntity<String> borrarTodo()
    {
        ResponseEntity<String> resultado = null;
        listaProductos.clear();
        /* como que no funcionó
        for (Producto p:listaProductos) {
            listaProductos.remove(p);
        }*/
        resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return resultado;
    }
}
