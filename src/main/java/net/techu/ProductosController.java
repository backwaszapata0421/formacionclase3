package net.techu;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductosController {

    private ArrayList<String> ListaProductos = null;

    public ProductosController() {
        ListaProductos = new ArrayList<>();
        ListaProductos.add("PR1");
        ListaProductos.add("PR2");
    }
/*Get lista de productos */
//    @RequestMapping(value = "/productos", method = RequestMethod.GET, produces = "application/json")
//se usa mejor GetMapping para evitar definir el metodo, mejores practicas
    @GetMapping(value = "/productos", produces = "application/json")
    public ArrayList<String> obtenerListado() {
        System.out.println("Estoy en Obtener");
        return ListaProductos;
    }
/* get de producto especifico */
    @RequestMapping(value = "/productos/{id}", method = RequestMethod.GET, produces = "application/json")
/*    public String obtenerProductoID(@PathVariable int id) {
        System.out.println("Estoy en Obtener Producto por ID");

// resultado son ningun control, solo esta linea
//      return ListaProductos.get(id);
// resultado capturando la ecepcion con try catch
        String resultado = null;
        try {
            resultado = ListaProductos.get(id);
        }
        catch (Exception ex){
            resultado = "No se ha encontrado el producto"
        }
        return resultado;  */
//Usar todos los Get con response Entity
    public ResponseEntity obtenerProductoID(@PathVariable int id)
    {
        String resultado = null;
        ResponseEntity respuesta = null;
        try
        {
            resultado = ListaProductos.get(id);
//son lo mismo las dos opciones de abajo para mostrar respuesta
            respuesta = new ResponseEntity<>(resultado, HttpStatus.OK);
//            respuesta = ResponseEntity.ok(resultado);
        }
        catch (Exception ex)
        {
            resultado = "No se ha encontrado el producto";
//son lo mismo las dos opciones de abajo
            respuesta = new ResponseEntity<>(resultado, HttpStatus.NOT_FOUND);
//            respuesta = ResponseEntity.notFound().build();
        }
        return respuesta;
    }


/* añade productos "en duro" */
    @RequestMapping(value = "/productos", method = RequestMethod.POST, produces = "application/json")
    public void AddProducto() {
        System.out.println("Estoy en Añadir");
        ListaProductos.add("Nuevo");
    }
/* añade productos desde ruta */
    @RequestMapping(value = "/productos/{nom}", method = RequestMethod.POST, produces = "application/json")
    public void AddProductoConNombre(@PathVariable("nom")String nombre) {
        System.out.println("Voy a añadir el producto con nombre" + nombre);
        ListaProductos.add(nombre);
    }
    @RequestMapping(value = "/productos/{nom}/{cat}", method = RequestMethod.POST, produces="application/json")
    public void AddProductoConNombre2(@PathVariable("nom") String nombre, @PathVariable("cat") String categoria) {
        System.out.println("Voy a añadir el producto con nombre " + nombre + " y categoría " + categoria);
        ListaProductos.add(nombre);
        //a pesar de obtener el nombre y la categoria, solo guarda el nombre
    }

/* Uso de Post con mejores practicas */
    @PostMapping(value = "/productos/a/{nom}", produces = "application/json")
    public ResponseEntity<String> addProducto() {
        System.out.println("Estoy en añadir");
        ListaProductos.add("NUEVO");
        return new ResponseEntity<>("Producto creado Correctamente", HttpStatus.CREATED);
    }
/* Uso de PUT con mejores practicas */
    @PutMapping(value = "/productos/{id}", produces = "application/json")
    public ResponseEntity<String> updateProducto(@PathVariable int id)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            String productoAModificar = ListaProductos.get(id);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }


    /* Uso de Delete con mejores practicas */
@DeleteMapping(value = "/productos/{id}")
public ResponseEntity delProductoID(@PathVariable int id)
{
    ResponseEntity<String> resultado = null;
    try
    {
        String delProductoID = ListaProductos.remove(id);
        resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    catch (Exception ex)
    {
        resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
    }
    return resultado;
}
}
