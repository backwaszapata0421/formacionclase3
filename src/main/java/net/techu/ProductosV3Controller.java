package net.techu;

import net.techu.data.ProductoMongo;
import net.techu.data.ProductoRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.List;

//@RestController
//@SpringBootApplication
public class ProductosV3Controller implements CommandLineRunner {

//    @Autowired
    private ProductoRepository repository;

    public static void main(String[] args) {
        SpringApplication.run(ProductosV3Controller.class, args);
    }
//    @Override
    public void run(String... args) throws Exception {
        System.out.println("Preparando Mongo");

        repository.deleteAll();
        repository.insert(new ProductoMongo("Walther",1));
        repository.insert(new ProductoMongo("WaltherS",2));
        repository.insert(new ProductoMongo("WaltherSZ",99));

        List<ProductoMongo> lista = repository.findAll();
        for(ProductoMongo p:lista){
            System.out.println(p.toString());
        }

//      repository.deleteAll();
//        repository.findByNombre("prueba");
    }

}
