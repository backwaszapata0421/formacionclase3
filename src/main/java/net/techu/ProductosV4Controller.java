package net.techu;

import net.techu.data.ProductoMongo;
import net.techu.data.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

//@SpringBootApplication
@RestController
public class ProductosV4Controller {

    @Autowired
    private ProductoRepository repository;

    @GetMapping(value = "/v4/productos", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> obtenerListado()
    {
        List<ProductoMongo> lista = repository.findAll();
        System.out.println("Estoy en Obtener lista v4");
        return new ResponseEntity<>(lista, HttpStatus.OK);
    }
    //ultimo prod por nombre con consultas query
    /* Get producto by nombre cuando es unico
    @GetMapping(value = "/v4/productosbynombre/{nombre}", produces = "application/json")
    public ResponseEntity<ProductoMongo> obtenerProductoPorNombre(@PathVariable String nombre)
    {
        ProductoMongo resultado = repository.findByNombre(nombre);
        return new ResponseEntity<ProductoMongo>(resultado, HttpStatus.OK);
    }*/
    //Lista
    /* Get producto by nombre */
    @GetMapping(value = "/v4/productosbynombre/{nombre}", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> obtenerProductoPorNombre(@PathVariable String nombre)
    {
        List<ProductoMongo> resultado = repository.findByNombre(nombre);
        return new ResponseEntity<List<ProductoMongo>>(resultado, HttpStatus.OK);
    }
    //Por precio
    @GetMapping(value="/v4/productosbyprecio/{minimo}/{maximo}")
    public ResponseEntity<List<ProductoMongo>> obtenerProductosPorPrecio(@PathVariable double minimo, @PathVariable double maximo)
    {
        List<ProductoMongo> resultado = repository.findByPrecio(minimo, maximo);
        return new ResponseEntity<List<ProductoMongo>>(resultado, HttpStatus.OK);
    }
    //mio
    /*
    @PostMapping(value = "/v4/productos/{nom}", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> addProducto() {
        System.out.println("Estoy en añadir");
        repository.insert(new ProductoMongo("WaltherNuevo",1));
        return new ResponseEntity("Producto creado Correctamente", HttpStatus.CREATED);
    }  */
    //profesor
    @PostMapping(value = "/v4/productos")
    public ResponseEntity<String> addProductoP(@RequestBody ProductoMongo productoMongo){
        ProductoMongo resultado = repository.insert(productoMongo);
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }
    @PutMapping(value = "/v4/productos/{id}", produces = "application/json")
    public ResponseEntity<String> updProductoP(@PathVariable String id, @RequestBody ProductoMongo productoMongo){
        Optional<ProductoMongo> resultado = repository.findById(String.valueOf(id));
        if (resultado.isPresent()){

            ProductoMongo productoAModificar = resultado.get();
            productoAModificar.nombre = productoMongo.nombre;
            productoAModificar.precio = productoMongo.precio;
            /* Optimizacion
            resultado.get().nombre = productoMongo.nombre;
            resultado.get().precio = productoMongo.precio; */
        }
        ProductoMongo guardado = repository.save(resultado.get());
        return new ResponseEntity<String>(resultado.toString(),HttpStatus.OK);
    }
    //mejorado por el profesor
    @PutMapping(value="/v4/productos/P/{id}")
    public ResponseEntity<String> updateProducto(@PathVariable String id, @RequestBody ProductoMongo productoMongo)
    {
        Optional<ProductoMongo> resultado = repository.findById(id);
        if (resultado.isPresent()) {
            ProductoMongo productoAModificar = resultado.get();
            productoAModificar.nombre = productoMongo.nombre;
            //productoAModificar.color = productoMongo.color;
            productoAModificar.precio = productoMongo.precio;
            ProductoMongo guardado = repository.save(resultado.get());
            return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
        }
        return new ResponseEntity<String>("Producto no encontrado", HttpStatus.NOT_FOUND);
    }
    @DeleteMapping(value="/v4/productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable String id){
        //Posibilidad de buscar el producto, eliminar si luego existe
        repository.deleteById(id);
        return new ResponseEntity<String>("Producto Borrado", HttpStatus.OK);
    }

}
